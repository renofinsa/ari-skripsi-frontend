import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt,faMoneyBill, faFile } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Alert from "./../../components/Alert";


import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'order'

class Index extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
    this.goBack = this.goBack.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  goBack(){
    this.props.history.goBack();
  }
   // handle delete
   onDelete(id) {
    console.log(id)
    // make delete request to the backend
    axios
    .delete(`${BASEURL.data}${URL}/${id}`)
    .then(res => {
      // alert.success('Success');
      window.location.href = '/menu'
      // this.props.history.push('/kamar')
    })
    .catch(err => console.log(err))
    }

  componentDidMount() {
    const { id } = this.props.match.params

  axios
  .get(`${BASEURL.data}${URL}/${id}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}


  render() {
    const { tb } = this.state
    const { id } = this.props.match.params

    var dataTh = [
      <th>#</th>,
      <th>Menu</th>,
      <th>Harga</th>,
      <th>Qty</th>,
      <th>Total Harga</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          <td>{a.label}</td>
          <td>Rp {a.harga_satuan}</td>
          <td>{a.qty}</td>
          <td>Rp {a.total_harga_item}</td>
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "/reservasi",
        value: "Reservasi"
      },
      {
        href: '#',
        value: "Invoice"
      },
      {
        href: '#',
        value: "Detail"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })

    

    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title=""
        btnAction={<Button onClick={this.goBack} className="btn btn-primary btn-custom">Kembali</Button>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>
    )
  }
}


export default Index;

import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Alert from "./../../components/Alert";


import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'kamar'

class Index extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
    this.onDelete = this.onDelete.bind(this)

  }


   // handle delete
   onDelete(id) {
    console.log(id)
    // make delete request to the backend
    axios
    .delete(`${BASEURL.data}${URL}/${id}`)
    .then(res => {
      // alert.success('Success');
      window.location.href = '/kamar'
      // this.props.history.push('/kamar')
    })
    .catch(err => console.log(err))
    }

  componentDidMount() {
  axios
  .get(`${BASEURL.data}${URL}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}


  render() {
    const { tb } = this.state

    var dataTh = [
      <th>#</th>,
      <th>Nomer Kamar</th>,
      <th>Kategori</th>,
      <th>Status</th>,
      <th>Aksi</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          <td>{a.nama}</td>
          <td>{a.kategori}</td>
          <td>{a.status}</td>
          <td> 
            <Button
              onClick={() => this.onDelete(a.id)}
              className="btn btn-danger btn-custom"
            >
              <FontAwesomeIcon icon={faTrash} />
            </Button>
            <Link
              to={`/kamar/update/${a.id}`} 
              className="btn btn-success btn-custom"
            >
              <FontAwesomeIcon icon={faPencilAlt} />
            </Link>
          </td>
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "#",
        value: "Kamar"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })

    

    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title="Kamar"
        btnAction={<Link to="/kamar/create" className="btn btn-primary btn-custom">Create</Link>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>

    )
  }
}


export default Index;

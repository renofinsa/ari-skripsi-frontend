import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'menu/kategori'

class Index extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
    this.onDelete = this.onDelete.bind(this)
  }

 // handle delete
 onDelete(id) {
  console.log(id)
  // make delete request to the backend
  axios
  .delete(`${BASEURL.data}${URL}/${id}`)
  .then(res => {
    // alert.success('Success');
    window.location.href = '/menu/kategori'
    // this.props.history.push('/kamar')
  })
  .catch(err => console.log(err))
  }

  componentDidMount() {
    
  axios
  .get(`${BASEURL.data}${URL}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}


  render() {
    const { tb } = this.state

    var dataTh = [
      <th>#</th>,
      <th>Label</th>,
      <th>Aksi</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          <td>{a.label}</td>
          <td> 
            <Button 
             onClick={() => this.onDelete(a.id)}
              className="btn btn-danger btn-custom"
            >
              <FontAwesomeIcon icon={faTrash} />
            </Button>
            <Link
              to={`/menu/kategori/update/${a.id}`} 
              className="btn btn-success btn-custom"
            >
              <FontAwesomeIcon icon={faPencilAlt} />
            </Link>
          </td>
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "#",
        value: "Kategori Menu"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })
    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title="Kategori Menu"
        btnAction={<Link to="/menu/kategori/create" className="btn btn-primary btn-custom">Create</Link>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>
    )
  }
}


export default Index;

import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt,faMoneyBill } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Alert from "./../../components/Alert";


import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'reservasi/list'

class Index extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
    this.onDelete = this.onDelete.bind(this)
  }


   // handle delete
   onDelete(id) {
    console.log(id)
    // make delete request to the backend
    axios
    .delete(`${BASEURL.data}${URL}/${id}`)
    .then(res => {
      // alert.success('Success');
      window.location.href = '/menu'
      // this.props.history.push('/kamar')
    })
    .catch(err => console.log(err))
    }
    
    // .get(`${BASEURL.data}${URL}`)
  componentDidMount() {
  axios
  .get(`${BASEURL.data}${URL}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}


  render() {
    const { tb } = this.state

    var dataTh = [
      <th>#</th>,
      <th>Tamu</th>,
      <th>Nomer Kamar</th>,
      <th>Tanggal Check In</th>,
      <th>Aksi</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          <td>{a.nama_lengkap}</td>
          <td>#{a.nomer_kamar}</td>
          <td>{a.tanggal_reservasi}</td>
          <td> 
           
            <Link
              to={`/reservasi/checkout/${a.tamu_id}`} 
              className="btn btn-dark btn-custom"
            >
              <FontAwesomeIcon icon={faMoneyBill} />
            </Link>
          </td>
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "#",
        value: "Reservasi"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })

    

    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title="Reservasi"
        btnAction={<Link to="/reservasi/create" className="btn btn-primary btn-custom">Buat Baru</Link>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>
    )
  }
}


export default Index;

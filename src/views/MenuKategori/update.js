import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Forms from './../../components/Form'
import './index.scss'
import {Link, Redirect}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'menu/kategori'


class index extends Component {
    constructor(props){
        super(props)
        this.state = {
            label: '',
            id: ''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        const payload = {
            label: this.state.label,
        }
        axios
        .patch(`${BASEURL.data}${URL}/${this.state.id}`, payload)
        .then(res => {
            console.log(res.data)
            // Redirect.to('/')
            window.location.href = '/menu/kategori'
        })
        .catch(err => {
            console.log(err)            
            this.setState({errors: err.response.data})})
    }

    componentDidMount(){
        const { id } = this.props.match.params
        axios
          .get(`${BASEURL.data}${URL}/${id}`)
          .then(res => {
            this.setState({
              id: res.data.data[0].id,
              label: res.data.data[0].label,
            })
            console.log(res.data.data[0].label)
          })
          .catch(err => this.setState({errors: err.response.data}))
           
    }



    render() {

        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/menu/kategori",
                value: "Kategori Menu"
              },
              {
                href: "#",
                value: "Update Kategori Menu"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} title="Update Kategori Kamar" />
               <div className="form-card">
               <form onSubmit={this.onSubmit}>

                <Forms 
                    label="Label"
                    type="text"
                    name="label"
                    placeholder="Single Bad"
                    value={this.state.label} 
                    onChange={this.onChange.bind(this)} 
                    />
                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
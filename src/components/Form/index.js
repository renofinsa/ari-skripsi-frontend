import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const Forms = (props) => {
    return (
        <div>
            <FormGroup>
                <Label for="label">{props.label}</Label>
                <Input 
                type={props.type} 
                name={props.name} 
                placeholder={props.placeholder} 
                value={props.value} 
                onChange={props.onChange} 
                />
            </FormGroup>
        </div>
    );
}

export default Forms;

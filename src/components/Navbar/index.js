import React, { Component } from 'react'
import {Link}  from 'react-router-dom';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

class headerNavbar extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
        <Link className="navbar-brand" to="/">Dashboard</Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <li className="nav-item">
              <Link className="nav-link" to="/reservasi">Reservasi</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/tamu">Data Tamu</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/pesanan">Pesanan</Link>
            </li>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                Kamar
                </DropdownToggle>
                <DropdownMenu right>
                  <Link className="dropdown-item" to="/kamar/kategori">Kategori Kamar</Link>
                  <Link className="dropdown-item" to="/kamar">Data Kamar</Link>
                </DropdownMenu>
              </UncontrolledDropdown>

            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                Menu
                </DropdownToggle>
                <DropdownMenu right>
                  <Link className="dropdown-item" to="/menu/kategori"> Menu Kategori </Link>
                  <Link className="dropdown-item" to="/menu">Data Menu</Link>
                </DropdownMenu>
              </UncontrolledDropdown>

              {/* <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Pesanan
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    History Pesanan
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> */}


              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  User
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Logout
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default headerNavbar
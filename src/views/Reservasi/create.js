import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Select from 'react-select';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'reservasi'
const URL_KATEGORI_KAMAR = 'kamar/tersedia'
const URL_TAMU = 'tamu/tersedia'



class index extends Component {
    constructor(){
        super()
        this.state = {
            catatan: '',
            
            tamu: [],
            tamu_id: {},
            kategori: [],
            kategori_label: '',
            kategori_id: {},

        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }

    onSelected = (kategori_id) => {
        this.setState({kategori_id})
        console.log(kategori_id)
    };

    onSelectedTamu = (tamu_id) => {
        this.setState({tamu_id})
        console.log(tamu_id)
    };

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        const payload = {
          kamar_id: this.state.kategori_id.value,
          tamu_id: this.state.tamu_id.value,
          catatan: this.state.catatan
        }
        console.log(payload)
    axios
      .post(`${BASEURL.data}${URL}`, payload)
      .then(res => {
        console.log(res)        
        this.props.history.push('/reservasi')
      }).catch(err => {
        console.log(err.response.data)
        this.setState({errors: err.response.data})
      })
       
    }

    componentDidMount(){
        axios
        .get(`${BASEURL.data}${URL_KATEGORI_KAMAR}`)
        .then(result => {
          console.log('tes', result.data.data)
          this.setState({
            kategori: result.data.data,
            loading: false
          })
        })
        .catch(error => {
          console.log(error.message, 'ini error')
          this.setState({
            error: error.message,
            loading: false
          })
        })

        axios
        .get(`${BASEURL.data}${URL_TAMU}`)
        .then(result => {
          console.log('tes', result.data.data)
          this.setState({
            tamu: result.data.data,
            loading: false
          })
        })
        .catch(error => {
          console.log(error.message, 'ini error')
          this.setState({
            error: error.message,
            loading: false
          })
        })
    }

    render() {
        const { 
            catatan,
            kategori_id,
            tamu_id
         } = this.state

         const ThisKategori = this.state.kategori.map(a => {
            return (
              {value: a.id, label: `${a.nama}| ${a.kategori}`}
            )
          })

         const ThisTamu = this.state.tamu.map(a => {
            return (
              {value: a.id, label: `${a.nama_lengkap}| ${a.nomer_telepon}`}
            )
          })

        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/reservasi",
                value: "Reservasi"
              },
              {
                href: "#",
                value: "Tambah"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} 
                title="" />
               <div className="form-card">
               <form onSubmit={this.onSubmit}>

                <div className="form-group">
                    <label>Tamu</label>
                    <Select 
                        value={tamu_id}
                        onChange={this.onSelectedTamu}
                        options={ ThisTamu } 
                    />
                </div>

                <div className="form-group">
                    <label>Kamar</label>
                    <Select 
                        value={kategori_id}
                        onChange={this.onSelected}
                        options={ ThisKategori } 
                    />
                </div>
                <div className="form-group">
                  <label>Catatan</label>
                  <textarea
                  name="catatan"
                  className="form-control"
                  value={catatan} 
                  onChange={this.onChange.bind(this)} 
                  >

                  </textarea>
                </div>
                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
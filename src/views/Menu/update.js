import React, { Component, useState, useEffect } from 'react'
import { useAlert } from "react-alert";
import HeaderTable from './../../components/HeaderTable'
import Select from 'react-select';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'menu'
const URL_KATEGORI = 'menu/kategori'


const Update = ({ history, match }) => {
  const alert = useAlert();
  
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(true)
  const [form, setForm] = useState({
    label: '',
    file: null,
    oldCover: null,
    deskripsi: '',
    harga: '',
    categories: [],
    categoryLabel: '',
    categoryId: -1,
  })

  let dataBreadItem = [
    {
        href: "/menu",
        value: "Menu"
      },
      {
        href: "#",
        value: "Update Menu"
      },
  ] 

  const onSelected = ({ value }) => {
    setForm({...form, categoryId: value})
  }

  const onChangeFile = e => {
    console.log(e.target.files) 
    this.setForm({...form, [e.target.name]: e.target.files[0]})
  }

  const onChange = e => {
    setForm({...form, [e.target.name]: e.target.value})
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const { id } = match.params
    let data = new FormData()
    data.append('file', form.file)
    data.append('label', form.label)
    data.append('kategori_menu_id', form.categoryId)
    data.append('harga', form.harga)
    data.append('deskripsi', form.deskripsi)
    data.append('oldCover', form.oldCover)

    console.log('------------')
    console.log(data)
    console.log('------------')
    
    axios
      .patch(`${BASEURL.data}${URL}/${id}`, data)
      .then(res => {
        // alert.success('Success');
        // window.location.href = '/kamar'
        history.push('/menu')
      })
      .catch(err => setError(err.response.data))
   
}

  useEffect(() => {
    const { id } = match.params
    let fr = {}
    axios
    .get(`${BASEURL.data}${URL_KATEGORI}`)
    .then(result => {
      console.log('tes', result.data.data)
      fr = {...form, categories: result.data.data}
      setForm(form)
    })
    .catch(error => {

    })
    .then(() => {
      return axios
      .get(`${BASEURL.data}${URL}/detail/${id}`)
    })
    .then(result => {
      console.log('tes', result.data.data)
      // setForm({
      //   nama: result.data.data.name,
      //   categoryId: result.data.data.kategori_id
      // })
      setForm({...fr,
        label           : result.data.data.label,
        deskripsi      : result.data.data.deskripsi,
        harga          : result.data.data.harga,
        oldCover       : result.data.data.cover,
        categoryId     : result.data.data.kategori_id,
        categoryLabel  : result.data.data.kategori,
        loading: false
      })
    })
    .catch(error => {
      console.log(error.message, 'ini error')
      setError(error.message)
      setLoading(false)
    })
  }, [])

  dataBreadItem = dataBreadItem.map((a, i) => <Link key={i} className="breadcrumb-item" to={a.href}>{a.value}</Link>)


  console.log(form)

  let cat = form.categories.map((a, i) => ({key:i, value: a.id, label: a.label}))


  return(
    <div className="body">
      <HeaderTable BreadItem={dataBreadItem} title="Update Kamar" />
      <div className="form-card">
      <form onSubmit={onSubmit}>

      <div className="row">
        <div className="col-md-6">
          <img className="size" src={`http://localhost:3002/images/menu/${form.oldCover}`} />
        </div>
        <div className="col-md-6">
        <Forms 
                    label="Upload Gambar"
                    type="file"
                    name="file"
                    value={form.file} 
                    onChange={onChangeFile} 
                    require
                    />
          <input
          label="Label"
          type="text"
          name="file"
          placeholder="Nasi Goreng"
          value={form.oldCover} 
          onChange={onChange} 
          ></input>
        </div>
      </div>

      <Forms 
          label="Menu"
          type="text"
          name="label"
          placeholder="Nasi Goreng"
          value={form.label} 
          onChange={onChange} 
          />
      

      <div className="form-group">
          <label>Kategori Kamar</label>
          <Select 
              name="form-field-name"
              value={cat.find(d => d.value === form.categoryId)}
              onChange={onSelected}
              options={ cat } 
          />
      </div>

          <Forms 
          label="Label"
          type="text"
          name="harga"
          placeholder="10000"
          value={form.harga} 
          onChange={onChange} 
          />

          <div className="form-group">
            <label>Deskripsi</label>
            <textarea
            value={form.deskripsi} 
            onChange={onChange} 
            className="form-control"
            label="Label"
            type="text"
            name="deskripsi"
            placeholder="tulis deskripsi makanan disini ..."
            require
            >
            </textarea>
          </div>

          <button className="btn btn-primary form-control">Simpan</button>
      </form>

      </div>
  </div>
  )
}

export default Update

// class index extends Component {
//     constructor(){
//         super()
//         this.state = {
//             nama: '',
            
//             kategori: [],
//             kategori_label: '',
//             kategori_id: {},

//         }
//         this.onChange = this.onChange.bind(this)
//         this.onSubmit = this.onSubmit.bind(this)

//     }

//     onSelected = (kategori_id) => {
//         this.setState({kategori_id})
//         console.log(kategori_id)
//     };

//     onChange(e){
//         this.setState({[e.target.name]: e.target.value})
//     }

//     onSubmit(e){
//         e.preventDefault()
//         const payload = {
//             nama: this.state.nama,
//             kategori_kamar_id: this.state.kategori_id.value,
//         }
//         console.log(payload)
//         // this.props.history.push('/kamar')
//     // axios
//     //   .post(`${BASEURL.data}${URL}`, payload)
//     //   .then(res => {
//     //     window.location.href = '/kamar'
//     //   })
//     //   .catch(err => this.setState({errors: err.response.data}))
       
//     }

//     componentDidMount(){
//         axios
//         .get(`${BASEURL.data}${URL_KATEGORI_KAMAR}`)
//         .then(result => {
//           console.log('tes', result.data.data)
//           this.setState({
//             kategori: result.data.data,
//             loading: false
//           })
//         })
//         .catch(error => {
//           console.log(error.message, 'ini error')
//           this.setState({
//             error: error.message,
//             loading: false
//           })
//         })

//         const { id } = this.props.match.params
//         axios
//         .get(`${BASEURL.data}${URL}/detail/${id}`)
//         .then(result => {
//           console.log('tes', result.data.data)
//           this.setState({
//             nama           : result.data.data.nama,
//             kategori_id    : result.data.data.kategori_id,
//             kategori_label : result.data.data.kategori,
//             loading: false
//           })
//         })
//         .catch(error => {
//           console.log(error.message, 'ini error')
//           this.setState({
//             error: error.message,
//             loading: false
//           })
//         })
//     }

//     render() {
//         const { 
//             nama,
//             kategori,
//             kategori_label,
//             kategori_id
//          } = this.state

//          const ThisKategori = this.state.kategori.map(a => {
//             return (
//               {value: a.id, label: a.label}
//             )
//           })

//          const ThisSelectedKategori = [
//             {
//               value : kategori_id,
//               label : kategori_label
//             }
//          ]
// console.log(ThisSelectedKategori[0])
//         var PushDataBreadItem = []
//         var DataBreadItem = [
//             {
//                 href: "/kamar",
//                 value: "Kamar"
//               },
//               {
//                 href: "#",
//                 value: "Update Kamar"
//               },
//         ] 
    
//         DataBreadItem.map(a => <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>)
//         return (
//             <div className="body">
//                 <HeaderTable BreadItem={DataBreadItem} title="Update Kamar" />
//                <div className="form-card">
//                <form onSubmit={this.onSubmit}>

//                 <Forms 
//                     label="Label"
//                     type="text"
//                     name="nama"
//                     placeholder="301"
//                     value={nama} 
//                     onChange={this.onChange.bind(this)} 
//                     />

//                 <div className="form-group">
//                     <label>Kategori Kamar</label>
//                     <Select 
//                         name="form-field-name"
//                         value={ThisKategori.find(d => d.value === kategori_id)}
//                         onChange={this.onSelected}
//                         options={ ThisKategori } 
//                     />
//                 </div>

//                     <button className="btn btn-primary form-control">Simpan</button>
//                 </form>

//                </div>
//             </div>
//         )
//     }
// }

// export default index
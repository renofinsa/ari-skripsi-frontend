// App.js
import React from 'react'
import { useAlert } from 'react-alert'
 
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import Alert from './components/Alert'


const options = {
    timeout: 5000,
    position: positions.BOTTOM_CENTER
  };
  
  const App = () => (
    <AlertProvider template={AlertTemplate} {...options}>
      <Alert />
    </AlertProvider>
  );
 
export default App
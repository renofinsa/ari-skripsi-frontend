import React from 'react';
import { Breadcrumb, BreadcrumbItem, Row, Col, Container } from 'reactstrap';
import './index.scss'
import {Link} from 'react-router-dom';


const HeaderTable = (props) => {
    return (
        <div>
            <div>
                <Breadcrumb tag="nav" listTag="div">
                    <Link to="/" className="breadcrumb-item">Home</Link>
                    {props.BreadItem}
                </Breadcrumb>
                <Container>

                <Row className="row-margin">
                    <Col md="6">
                        <h3>{props.title}</h3>
                    </Col>
                    <Col md="6">
                        {props.btnAction}
                    </Col>
                </Row>
                </Container>

            </div>
        </div>
    );
};

export default HeaderTable;

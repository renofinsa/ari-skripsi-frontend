import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'order/list'

class Tamu extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
  }


  componentDidMount() {
  axios
  .get(`${BASEURL.data}${URL}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}



  render() {
    const { tb } = this.state

    var dataTh = [
      <th>#</th>,
      // <th>Invoice</th>,
      <th>Pemesan</th>,
      <th>Menu</th>,
      <th>Qty</th>,
      <th>Catatan</th>,
      <th>Estimasi</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          {/* <td>{a.invoice}</td> */}
          <td>{a.nama_lengkap}| {a.nomer_telepon}</td>
          <td>{a.label}</td>
          <td>{a.qty}</td>
          <td>{a.catatan}</td>
          <td>{a.estimasi}</td>
          
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "#",
        value: "Pesanan"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })



    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title="Data Pesanan"
        btnAction={<Link to='/pesanan/history' className="btn btn-primary btn-custom">Riwayat Pesanan</Link>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>
    )
  }
}


export default Tamu;

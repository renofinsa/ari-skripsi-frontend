import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import Select from 'react-select';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'menu'
const URL_KATEGORI = 'menu/kategori'



class index extends Component {
    constructor(){
        super()
        this.state = {
            label: '',
            deskripsi: '',
            harga: '',
            file: null,
            kategori: [],
            kategori_label: '',
            kategori_id: {},

        }
        this.onChangeFile = this.onChangeFile.bind(this)
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }

    onChangeFile(e) {
      console.log(e.target.files) 
      this.setState({[e.target.name]: e.target.files[0]})
    }

    onSelected = (kategori_id) => {
        this.setState({kategori_id})
        console.log(kategori_id)
    };

    onChange(e){
      console.log(`${e.target.name} ${e.target.value}`)

        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        let data = new FormData()
        data.append('file', this.state.file)
        data.append('label', this.state.label)
        data.append('kategori_menu_id', this.state.kategori_id.value)
        data.append('harga', this.state.harga)
        data.append('deskripsi', this.state.deskripsi)

        console.log('------------')
        console.log(data)
        console.log('------------')
        axios
          .post(`${BASEURL.data}${URL}`, data)
          .then(res => {
console.log(res)
            this.props.history.push('/menu')
          })
          .catch(err => this.setState({errors: err.response.data}))
    }

    componentDidMount(){
        axios
        .get(`${BASEURL.data}${URL_KATEGORI}`)
        .then(result => {
          console.log('tes', result.data.data)
          this.setState({
            kategori: result.data.data,
            loading: false
          })
        })
        .catch(error => {
          console.log(error.message, 'ini error')
          this.setState({
            error: error.message,
            loading: false
          })
        })
    }

    render() {
        const { 
            nama,
            deskripsi,
            harga,
            kategori,
            kategori_id
         } = this.state

         const ThisKategori = this.state.kategori.map(a => {
            return (
              {value: a.id, label: a.label}
            )
          })

        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/menu",
                value: "Kamar"
              },
              {
                href: "#",
                value: "Tambah Menu"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} title="Tambah Menu" />
               <div className="form-card">
               <form onSubmit={this.onSubmit} enctype="multipart/form-data">

                <Forms 
                    label="Upload Gambar"
                    type="file"
                    name="file"
                    value={nama} 
                    onChange={this.onChangeFile.bind(this)} 
                    require
                    />
                <Forms 
                    label="Menu"
                    type="text"
                    name="label"
                    placeholder="Nasi Goreng"
                    value={nama} 
                    onChange={this.onChange.bind(this)} 
                    require
                    />

                <div className="form-group">
                    <label>Kategori Menu</label>
                    <Select 
                        value={kategori_id}
                        onChange={this.onSelected}
                        options={ ThisKategori } 
                    />
                </div>
                <Forms 
                    label="Harga"
                    type="text"
                    name="harga"
                    placeholder="10000"
                    value={harga} 
                    onChange={this.onChange.bind(this)} 
                    require
                    /> 

                  <div className="form-group">
                    <label>Deskripsi</label>
                    <textarea
                    value={deskripsi} 
                    onChange={this.onChange.bind(this)} 
                    className="form-control"
                    label="Label"
                    type="text"
                    name="deskripsi"
                    placeholder="tulis deskripsi makanan disini ..."
                    require
                    >
                    </textarea>
                  </div>

                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
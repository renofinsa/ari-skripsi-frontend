import React, { Component } from 'react'
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Navbar from './components/Navbar'
import Tamu from './views/Tamu'
import CreateTamu from './views/CreateTamu'
import Kamar from './views/Kamar'
import KamarCreate from './views/Kamar/create'
import KamarUpdate from './views/Kamar/update'
import KategoriKamar from './views/KategoriKamar'
import CreateKategoriKamar from './views/CreateKategoriKamar'
import UpdateKategoriKamar from './views/CreateKategoriKamar/update'

import Menu from './views/Menu'
import MenuCreate from './views/Menu/create'
import MenuUpdate from './views/Menu/update'

import MenuImage from './views/Menu/gambar'
import MenuImageUploads from './views/Menu/uploads'

import Reservasi from './views/Reservasi'
import ReservasiCreate from './views/Reservasi/create'
import Checkout from './views/Reservasi/checkout'
import CheckoutDetail from './views/Reservasi/detail'
import Pesanan from './views/Order/index'
import History from './views/Order/history'
import HistoryDetail from './views/Order/detail'

import KategoriMenu from './views/MenuKategori'
import CreateKategoriMenu from './views/MenuKategori/create'
import UpdateKategoriMenu from './views/MenuKategori/update'
import './index.scss'
import { Container } from 'reactstrap';


import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

class App extends Component {
 
  render() {
    

    const options = {
      timeout: 5000,
      position: positions.BOTTOM_RIGHT
    };
    return (
      <Provider template={AlertTemplate} {...options}>
      <div>
        <Router>
        <div>
          <Navbar />
            <Route exact path="/" component={ Tamu } />
            <Container>
              <Route exact path="/tamu" component={ Tamu } />
              <Route exact path="/tamu/create" component={ CreateTamu } />
              <Route exact path="/kamar" component={ Kamar } />
              <Route exact path="/kamar/create" component={ KamarCreate } />
              <Route exact path="/kamar/update/:id" component={ KamarUpdate } />
              <Route exact path="/kamar/kategori" component={ KategoriKamar } />
              <Route exact path="/kamar/kategori/create" component={ CreateKategoriKamar } />
              <Route exact path="/kamar/kategori/update/:id" component={ UpdateKategoriKamar } />
              
              <Route exact path="/menu" component={ Menu } />
              <Route exact path="/menu/create" component={ MenuCreate } />
              <Route exact path="/menu/update/:id" component={ MenuUpdate } />

              <Route exact path="/menu/images/:id" component={ MenuImage } />
              <Route exact path="/menu/images/:id/uploads" component={ MenuImageUploads } />
              
              <Route exact path="/reservasi" component={ Reservasi } />
              <Route exact path="/reservasi/create" component={ ReservasiCreate } />
              <Route exact path="/reservasi/checkout/:id" component={ Checkout } />
              <Route exact path="/reservasi/checkout/detail/:id" component={ CheckoutDetail } />
             
              <Route exact path="/menu/kategori" component={ KategoriMenu } />
              <Route exact path="/menu/kategori/create" component={ CreateKategoriMenu } />
              <Route exact path="/menu/kategori/update/:id" component={ UpdateKategoriMenu } />
             
              <Route exact path="/pesanan" component={ Pesanan } />
              <Route exact path="/pesanan/history" component={ History } />
              <Route exact path="/pesanan/history/:id" component={ HistoryDetail } />
            </Container>
        </div>
      </Router>
      </div>
      </Provider>
    )
  }
}


export default App;

import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Select from 'react-select';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'menu/images'
const URL_KATEGORI = 'menu/kategori'



class index extends Component {
    constructor(){
        super()
        this.state = {
            nama: '',
            file: null
        }
        this.onChangeFile = this.onChangeFile.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)

    }
    onChange(e){
      this.setState({[e.target.name]: e.target.value})
  }
    onChangeFile(e) {
      console.log(e.target.files) 
      this.setState({[e.target.name]: e.target.files[0]})
    }

    onSubmit(e){
      const { id } = this.props.match.params

        e.preventDefault()
        let data = new FormData()
        data.append('file', this.state.file)
        data.append('cover', this.state.nama)
        data.append('menu_id', id)
        
        console.log('-------------')
        console.log(data)
        console.log('-------------')
        axios
          .post(`${BASEURL.data}${URL}`, data)
          .then(res => {
            this.props.history.goBack()
          })
          .catch(err => this.setState({errors: err.response.data}))
    }

    

    render() {
    const { id } = this.props.match.params
        const { 
            nama,
            file
         } = this.state

        

        var PushDataBreadItem = []
        var DataBreadItem = [
          {
            href: "/menu",
            value: "Menu"
          },
          {
            href: `/menu/images/${id}`,
            value: "Gambar"
          },
          {
            href: "#",
            value: "Unggah"
          },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} 
                />
               <div className="form-card">
               <form onSubmit={this.onSubmit} enctype="multipart/form-data">

                <Forms 
                    label="Label"
                    type="file"
                    name="file"
                    onChange={this.onChangeFile.bind(this)} 
                    require
                    />

                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
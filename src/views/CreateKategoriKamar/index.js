import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'kamar/kategori'


class index extends Component {
    constructor(){
        super()
        this.state = {
            label: ''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        const payload = {
            label: this.state.label,
        }
        console.log(payload)
    axios
      .post(`${BASEURL.data}${URL}`, payload)
      .then(res => {
          this.props.history.push('/kamar/kategori')
        // window.location.href = '/kamar/kategori'
      })
      .catch(err => this.setState({errors: err.response.data}))
       
    }

    render() {

        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/kamar/kategori",
                value: "Data Kategori Kamar"
              },
              {
                href: "#",
                value: "Tambah Kategori Kamar"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} title="Tambah Kategori Kamar" />
               <div className="form-card">
               <form onSubmit={this.onSubmit}>

                <Forms 
                    label="Label"
                    type="text"
                    name="label"
                    placeholder="Single Bad"
                    value={this.state.name} 
                    onChange={this.onChange.bind(this)} 
                    />
                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
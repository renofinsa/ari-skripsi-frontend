import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'auth/register'

class index extends Component {
    constructor(){
        super()
        this.state = {
            nama_lengkap: '',
            nomer_telepon: '',
            email: '',
            jenis_kelamin: '',
            kata_sandi: '',
            alamat: '',
            ktp: '',
            tempat_tanggal_lahir: '',
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }
    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        console.log(e)
        const payload = {
            nama_lengkap : this.state.nama_lengkap,
            nomer_telepon : this.state.nomer_telepon,
            email : this.state.email,
            jenis_kelamin : this.state.jenis_kelamin,
            kata_sandi : this.state.kata_sandi,
            alamat : this.state.alamat,
            ktp : this.state.ktp,
            tempat_tanggal_lahir : this.state.tempat_tanggal_lahir,
        }
        console.log(payload)
    axios
      .post(`${BASEURL.data}${URL}`, payload)
      .then(res => {
          console.log(res)
          this.props.history.push('/tamu')
        // window.location.href = '/kamar/kategori'
      })
      .catch(err => {
          console.log(err)
        this.setState({errors: err.response.data})
      })
       
    }

    render() {
        const {
            nama_lengkap,
            nomer_telepon,
            email,
            jenis_kelamin,
            kata_sandi,
            alamat,
            ktp,
            tempat_tanggal_lahir
        } = this.state

        console.log('jk', jenis_kelamin)
        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/tamu",
                value: "Data Tamu"
              },
              {
                href: "#",
                value: "Create Tamu"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} title="Create Tamu" />
               <div className="form-card">
               <form onSubmit={this.onSubmit}>

                    <Forms 
                    label="Nama Lengkap"
                    type="text"
                    name="nama_lengkap"
                    placeholder="Nama Lengkap"
                    value={nama_lengkap} 
                    onChange={this.onChange.bind(this)} 
                    />
                    <Forms 
                    label="Nomer Telepon"
                    type="text"
                    name="nomer_telepon"
                    placeholder="Nomer Telepon"
                    value={nomer_telepon} 
                    onChange={this.onChange.bind(this)} 

                    />
                    <Forms 
                    label="Email"
                    type="text"
                    name="email"
                    placeholder="email@domain.com"
                    value={email} 
                    onChange={this.onChange.bind(this)} 

                    />
                    <FormGroup>
                    <Label for="label" sm={4}>Jenis Kelamin</Label>
                    <Row>
                        <Col md="3">
                            <FormGroup check>
                                <Label check>
                                    <Input value={1} onChange={this.onChange.bind(this)}  type="radio" name="jenis_kelamin" checked={jenis_kelamin === '1'} />{' '}
                                    Tuan
                                </Label>
                            </FormGroup>
                        </Col>
                        <Col md="3">
                            <FormGroup check>
                                <Label check>
                                    <Input value={0} onChange={this.onChange.bind(this)}  type="radio" name="jenis_kelamin" checked={jenis_kelamin === '0'}/>{' '}
                                    Nyonya
                                </Label>
                            </FormGroup>
                        </Col>
                    </Row>
                    </FormGroup>
                    <Forms 
                    label="Kata Sandi"
                    type="text"
                    name="kata_sandi"
                    placeholder="123"
                    value={kata_sandi} 
                    onChange={this.onChange.bind(this)} 

                    />
                    <Forms 
                    label="Nomer KTP/ SIM"
                    type="text"
                    name="ktp"
                    placeholder="123456789"
                    value={ktp} 
                    onChange={this.onChange.bind(this)} 
                    />
                    <Forms 
                    label="Tempat, Tanggal Lahir"
                    type="text"
                    name="tempat_tanggal_lahir"
                    placeholder="Jakarta, 01 Januari 1990"
                    value={tempat_tanggal_lahir} 
                    onChange={this.onChange.bind(this)} 
                    />
                    <div className="form-group">
                        <label>Alamat</label>
                        <textarea
                        className="form-control"
                        name="alamat"
                        placeholder="alamat tamu sesuai ktp"
                        value={alamat} 
                        onChange={this.onChange.bind(this)} 
                        >
                        </textarea>
                    </div>
                    
                    <button className="btn btn-primary form-control">Simpan</button>
               </form>
               </div>
            </div>
        )
    }
}

export default index
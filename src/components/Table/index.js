import React, { Component } from 'react'
import { Table } from 'reactstrap';

class Tables extends Component {
    constructor(props){
        super(props)
    }


  render() {
    return (
      <Table size="sm">
        <thead>
          <tr>
            {this.props.th}
          </tr>
        </thead>
        <tbody>
            {this.props.tb}
        </tbody>
      </Table>
    );
  }
}
export default Tables
import React, { Component } from 'react'
import HeaderTable from './../../components/HeaderTable'
import { BreadcrumbItem, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Select from 'react-select';
import Forms from './../../components/Form'
import './index.scss'
import {Link}  from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'kamar'
const URL_KATEGORI_KAMAR = 'kamar/kategori'



class index extends Component {
    constructor(){
        super()
        this.state = {
            nama: '',
            
            kategori: [],
            kategori_label: '',
            kategori_id: {},

        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }

    onSelected = (kategori_id) => {
        this.setState({kategori_id})
        console.log(kategori_id)
    };

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()
        const payload = {
            nama: this.state.nama,
            kategori_kamar_id: this.state.kategori_id.value,
        }
        console.log(payload)
    axios
      .post(`${BASEURL.data}${URL}`, payload)
      .then(res => {
        window.location.href = '/kamar'
      })
      .catch(err => this.setState({errors: err.response.data}))
       
    }

    componentDidMount(){
        axios
        .get(`${BASEURL.data}${URL_KATEGORI_KAMAR}`)
        .then(result => {
          console.log('tes', result.data.data)
          this.setState({
            kategori: result.data.data,
            loading: false
          })
        })
        .catch(error => {
          console.log(error.message, 'ini error')
          this.setState({
            error: error.message,
            loading: false
          })
        })
    }

    render() {
        const { 
            nama,
            kategori,
            kategori_id
         } = this.state

         const ThisKategori = this.state.kategori.map(a => {
            return (
              {value: a.id, label: a.label}
            )
          })

        var PushDataBreadItem = []
        var DataBreadItem = [
            {
                href: "/kamar",
                value: "Kamar"
              },
              {
                href: "#",
                value: "Tambah Kamar"
              },
        ] 
    
        DataBreadItem.map(a => {
          PushDataBreadItem.push(
          <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
          )
        })
        return (
            <div className="body">
                <HeaderTable BreadItem={PushDataBreadItem} title="Tambah Kamar" />
               <div className="form-card">
               <form onSubmit={this.onSubmit}>

                <Forms 
                    label="Label"
                    type="text"
                    name="nama"
                    placeholder="301"
                    value={nama} 
                    onChange={this.onChange.bind(this)} 
                    require
                    />

                <div className="form-group">
                    <label>Kategori Kamar</label>
                    <Select 
                        value={kategori_id}
                        onChange={this.onSelected}
                        options={ ThisKategori } 
                    />
                </div>

                    <button className="btn btn-primary form-control">Simpan</button>
                </form>

               </div>
            </div>
        )
    }
}

export default index
import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt,faMoneyBill, faFile } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Alert from "./../../components/Alert";

import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'order/unpaid/user'
const URLCHECKOUT = 'reservasi/paid'
const URLAMBILTOTAL = 'order/unpaid/user/total'

class Index extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : [],
      total : []
    }
    this.onCheckout = this.onCheckout.bind(this)
  }

   // handle delete
   onCheckout(id) {
      console.log(id)
      axios
      .patch(`${BASEURL.data}${URLCHECKOUT}/${id}`)
      .then(res => {
        // alert.success('Success');
        this.props.history.push('/reservasi')
      })
      .catch(err => console.log(err))
    }

  componentDidMount() {
    const { id } = this.props.match.params

    axios
    .get(`${BASEURL.data}${URL}/${id}`)
    .then(result => {
      console.log('tes', result.data.data)
      this.setState({
        tb: result.data.data,
        loading: false
      })
    })
    .catch(error => {
      console.log(error.message, 'ini error')
      this.setState({
        error: error.message,
        loading: false
      })
    })
    // AMBIL TOTAL
    axios
    .get(`${BASEURL.data}${URLAMBILTOTAL}/${id}`)
    .then(result => {
      console.log('ambil total', result.data.data[0])
      this.setState({
        total: result.data.data[0],
        loading: false
      })
    })
    .catch(error => {
      console.log(error.message, 'ini error')
      this.setState({
        error: error.message,
        loading: false
      })
    })
  }


  render() {
    const { id } = this.props.match.params
    const { tb, total } = this.state

    var dataTh = [
      <th>#</th>,
      <th>Invoice</th>,
      <th>Total Harga</th>,
      <th>Qty</th>,
      <th>Aksi</th>,
    ]
    var pushDataTB = []

    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          <td>{a.invoice}</td>
          <td>Rp {a.total_harga}</td>
          <td>{a.total_qty}</td>
          <td> 
            <Link
              to={`/reservasi/checkout/detail/${a.invoice}`} 
              className="btn btn-dark btn-custom"
            >
              <FontAwesomeIcon icon={faFile} />
            </Link>
          </td>
        </tr>
      )
    })

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "/reservasi",
        value: "Reservasi"
      },
      {
        href: "#",
        value: "Invoice"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })

    
    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title=""
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  /> 
          <div className="form-group align-right">
            <h6>Total Harga : Rp {total.total_harga}</h6>
          </div>

          <Button
              onClick={() => this.onCheckout(id)}
              className="btn btn-success form-control btn-custom"
          >Checkout</Button>
        </Container>  
      </div>
    )
  }
}


export default Index;

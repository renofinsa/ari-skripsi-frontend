import React, { Component } from 'react'
import { Button, Container, BreadcrumbItem } from 'reactstrap';
import Table from './../../components/Table'
import HeaderTable from './../../components/HeaderTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import {Link} from 'react-router-dom';
import axios from 'axios'
import BASEURL from './../../utils/env.json'
const URL = 'order/paid/history'

class Tamu extends Component {
  constructor(props){
    super(props)
    this.state = {
      th : [],
      tb : []
    }
    this.goBack = this.goBack.bind(this)
  }

  goBack(){
    this.props.history.goBack();
  }


  componentDidMount() {
  axios
  .get(`${BASEURL.data}${URL}`)
  .then(result => {
    console.log('tes', result.data.data)
    this.setState({
      tb: result.data.data,
      loading: false
    })
  })
  .catch(error => {
    console.log(error.message, 'ini error')
    this.setState({
      error: error.message,
      loading: false
    })
  })
}



  render() {
    const { tb } = this.state
    
    var dataTh = [
      <th>#</th>,
      <th>invoice</th>,
      <th>Pemesan</th>,
      <th>Qty</th>,
      <th>Total Harga</th>,
      <th>Jam/Tanggal</th>,
    ]
    var pushDataTB = []
    tb.map((a, index) => {
      pushDataTB.push(
        <tr>
          <th scope="row">{index+1}</th>
          {/* <td>{a.invoice}</td> */}
          <td><Link to={`/pesanan/history/${a.invoice}`}>{a.invoice}</Link></td>
          <td>{a.nama_lengkap}| {a.nomer_telepon}</td>
          <td>{a.total_qty}</td>
          <td>Rp {a.total_harga}</td>
          <td>{a.createdAt}</td>
          
        </tr>
      )
    })
    // console.log(pushDataTB)

    var PushDataBreadItem = []
    var DataBreadItem = [
      {
        href: "/pesanan",
        value: "Pesanan"
      },
      {
        href: "#",
        value: "Riwayat"
      },
    ] 

    DataBreadItem.map(a => {
      PushDataBreadItem.push(
        <Link className="breadcrumb-item" to={a.href}>{a.value}</Link>
      )
    })



    return (
      <div className="body">
        <HeaderTable BreadItem={PushDataBreadItem} 
        title=""
        btnAction={<Button onClick={this.goBack} className="btn btn-primary btn-custom">Kembali</Button>}
         />
        <Container>
          <Table th={dataTh} tb={pushDataTB.map(a => a)}  />  
        </Container>  
      </div>
    )
  }
}


export default Tamu;
